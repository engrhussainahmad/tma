const express = require('express')
const logger = require('morgan')
const bodyParser = require('body-parser')
const cors = require("cors")

// Initialize Express
const app = express()

let corsOptions = {
    origin: 'http://127.0.0.1:8080'
}

// Allowed CORS Origin
app.use(cors(corsOptions))

// Log requests to the console.
app.use(logger('dev'))

// Parse incoming requests data
app.use(bodyParser.json())
app.use(bodyParser.urlencoded({ extended: false }))

require('./server/routes')(app)
app.get('*', (req, res) => res.status(200).send({
    message: 'Welcome to task management applications.'
}))

module.exports = app