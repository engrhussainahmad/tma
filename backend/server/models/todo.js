'use strict'
const {
    Model
} = require('sequelize')

module.exports = (sequelize, DataTypes) => {
    class Todo extends Model {
        /**
         * Helper method for defining associations.
         * This method is not a part of Sequelize lifecycle.
         * The `models/index` file will call this method automatically.
         */
        static associate(models) {
            Todo.hasMany(Todo, {
              foreignKey: 'todoId',
              as: 'subtasks'
            })
        }
    }

    Todo.init({
        title: DataTypes.STRING,
        status: DataTypes.ENUM(['pending', 'completed'])
    }, {
        sequelize,
        modelName: 'Todo',
        tableName: 'todos'
    })

    return Todo
}