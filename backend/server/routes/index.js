const todosController = require('../controllers').todos
const subtaskController = require('../controllers').subtask

module.exports = (app) => {
    // Todos routes
    app.get('/list', todosController.list)
    app.post('/todos', todosController.create)
    app.put('/todos/:todoId/update', todosController.update)

    // Subtasks routes
    app.post('/subtasks/:todoId/create', subtaskController.create)
    app.put('/subtasks/:todoId/update/:id', subtaskController.update)
}