'use strict'

const todos = [
  {
    title: 'Task one',
    status: 'pending',
    createdAt: new Date(),
    updatedAt: new Date()
  }, {
    title: 'Task two',
    status: 'pending',
    createdAt: new Date(),
    updatedAt: new Date()
  }, {
    title: 'Task three',
    status: 'pending',
    createdAt: new Date(),
    updatedAt: new Date()
  }, {
    title: 'Task four',
    status: 'pending',
    createdAt: new Date(),
    updatedAt: new Date()
  },
]

module.exports = {
  up: async (queryInterface, Sequelize) => {
    await queryInterface.bulkInsert('todos', todos, {})
  },

  down: async (queryInterface, Sequelize) => {
    await queryInterface.bulkDelete('todos', null, {})
  }
};
