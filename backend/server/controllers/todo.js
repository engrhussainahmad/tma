const Todo = require('../models').Todo

module.exports = {
    create(req, res) {
        return Todo
            .create({
                title: req.body.title,
                status: 'pending'
            })
            .then(todo => {
                todo.setDataValue('subtasks', [])
                return res.status(201).send(todo)
            })
            .catch(error => res.status(400).send(error))
    },
    list(req, res) {
        return Todo
            .findAll({
                where: {
                    todoId: null
                },
                include: [{
                    model: Todo,
                    as: 'subtasks'
                }],
            })
            .then(todo => res.status(201).send(todo))
            .catch(error => res.status(400).send(error))
    },
    update(req, res) {
        return Todo
            .findOne({
                where: {
                    id: req.params.todoId
                },
                include: [{
                    model: Todo,
                    as: 'subtasks',
                }]
            })
            .then(todo => {
                if (!todo) {
                    return res.status(404).send({
                        message: 'Todo Not Found',
                    })
                }
                return todo
                    .update({
                        status: req.body.status || todo.status
                    })
                    .then(() => {
                        Todo.update({status: req.body.status}, {where: {todoId: req.params.todoId}}).then(tasks => tasks)
                        return res.status(200).send(todo)
                    })
                    .catch((error) => res.status(400).send(error))
            })
            .catch((error) => res.status(400).send(error))
    }
}