const Todo = require('../models').Todo

module.exports = {
    create(req, res) {
        return Todo
            .create({
                title: req.body.title,
                todoId: req.params.todoId,
                status: 'pending'
            })
            .then(subtask => res.status(201).send(subtask))
            .catch(error => res.status(400).send(error))
    },
    update(req, res) {
        return Todo
            .findOne({
                where: {
                    id: req.params.id
                }
            })
            .then(subtask => {
                if (!subtask) {
                    return res.status(404).send({
                        message: 'Subtask Not Found',
                    })
                }
                return subtask
                    .update({
                        status: req.body.status || subtask.status
                    })
                    .then(() => res.status(200).send(subtask))
                    .catch((error) => res.status(400).send(error))
            })
    }
}