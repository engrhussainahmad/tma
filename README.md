##Setting up TMA(Task Management Application) on local machine

### Backend Setup

___Note:___ Unzip the file after downloading and make sure both the backend and frontend directories exist in the root folder.

Follow the following steps to set up backend on local machine.

1. Run the following command in inside backend directory:
    ```shell
    npm insall OR yarn
    ```
2. Create a postgres database with all the credentials as per the `config.json` file inside config folder.
3. Run the following migration and seeder command to build database schema and seed some dummy data:
    ```shell
    sequelize db:migrate && sequelize db:seed:all
    ```
4. After doing all the above stuffs, let us run the node server by using the following command:
    ```shell
    npm run start:dev OR yarn start:dev
    ```
   and the server will start running on port `8000`.

### Frontend Setup

After setting up backend and running successfully, now it's time to set frontend by following bellow steps:

1. Run the following command to download all the required packages:
   ```shell
   npm install OR yarn 
   ```
2. After install the dependent packages, let us start the application with:
    ```shell
    npm run serve || yarn serve
    ```
   and the application will run on:`http://localhost:8080`.

___Note:___ If the application is not automatically run on the browser, copy the above link and open it in your favorite browser and test the application.