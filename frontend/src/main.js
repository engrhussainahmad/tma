import { createApp } from 'vue'
import App from '@/App'
window.$ = window.jQuery = require('jquery')
require('bootstrap')
window._ = require('lodash')
window.toastr = require('toastr')
window.axios = require('axios')
window.axios.defaults.headers.common['Access-Control-Allow-Origin'] = '*'
window.axios.defaults.baseURL = process.env.VUE_APP_SERVER_URL

const app = createApp(App)
app.mount('#app')
